import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForButtonsPage } from './for-buttons.page';

describe('ForButtonsPage', () => {
  let component: ForButtonsPage;
  let fixture: ComponentFixture<ForButtonsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForButtonsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForButtonsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
