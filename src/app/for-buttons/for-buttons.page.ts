import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';



@Component({
  selector: 'app-for-buttons',
  templateUrl: './for-buttons.page.html',
  styleUrls: ['./for-buttons.page.scss'],
})
export class ForButtonsPage implements OnInit {
 public items : Array<{ title: string;}> = [{title:"1"},{title:"1"},{title:"1"}];
 public myText:String = "";
  constructor(private router: Router) { }

  ngOnInit() {
  }
  public search(type){
    this.router.navigate(["/list",{"type":type,"text":this.myText}]);
    console.log(type, this.myText);
    
  }

}
