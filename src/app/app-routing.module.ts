import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  { path: 'my-page', loadChildren: './my-page/my-page.module#MyPagePageModule' },
  { path: 'p1', loadChildren: './p1/p1.module#P1PageModule' },
  { path: 'p2', loadChildren: './p2/p2.module#P2PageModule' },  { path: 'image', loadChildren: './image/image.module#ImagePageModule' },
  { path: 'for-buttons', loadChildren: './for-buttons/for-buttons.module#ForButtonsPageModule' },
  { path: 'video', loadChildren: './video/video.module#VideoPageModule' },
  { path: 'chrono', loadChildren: './chrono/chrono.module#ChronoPageModule' },
  { path: 'laps', loadChildren: './laps/laps.module#LapsPageModule' },
  { path: 'top', loadChildren: './top/top.module#TopPageModule' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
