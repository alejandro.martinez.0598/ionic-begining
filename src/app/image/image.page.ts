import { Component, OnInit } from '@angular/core';
import { TypeImage } from '../type-image';
import { ActivatedRoute } from '@angular/router';
import { Movie } from '../movie';

@Component({
  selector: 'app-image',
  templateUrl: './image.page.html',
  styleUrls: ['./image.page.scss'],
})
export class ImagePage implements OnInit {

  public monFilm :Movie = {
    Poster: '',
    Title: '',
    Type: '',
    Year: '',
    imdbID: '',
  }
  constructor(private rout : ActivatedRoute) {
    this.rout.params.subscribe((params:Movie)=>{
      console.log(params);
      this.monFilm = params;
    })
   }

  ngOnInit() {
  }

}
