import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public items: Array<{ title: string; index:Number;  }> = [];
  public pages = ['/p1','/p2'];



    constructor(private router: Router){
      for (let i = 0; i < 3; i++) {
        this.items.push({
          title: 'Item ' + i,
         index: i
        });
      }
    }
    public method(pageIndex) {
      // cambiar de pagina
      this.router.navigate([this.pages[pageIndex],{'test':999,'back':'/home'}]);
      
    }
}
