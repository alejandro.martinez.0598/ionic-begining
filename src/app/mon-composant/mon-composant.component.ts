import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-mon-composant',
  templateUrl: './mon-composant.component.html',
  styleUrls: ['./mon-composant.component.scss']
})
export class MonComposantComponent implements OnInit {
  // parametres du composant    (ils sont  les props du Vue)
  @Input () depuis : string;
  constructor() { }

  ngOnInit() {
    console.log(this.depuis);
    
  }

}
