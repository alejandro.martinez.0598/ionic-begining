import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JsonConsumerService {

  private baseUrl: String = '/assets/json/auteurs.json';

  constructor(
      private readonly http: HttpClient ) {
    console.log('Dans Json Service');
  }

  getFrom(Url): Observable<any> {
    return this.http.get(`${Url}`);
  }
}
