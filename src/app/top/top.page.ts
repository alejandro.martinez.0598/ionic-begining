import { Component, OnInit } from '@angular/core';
import { MyTime } from '../MyTime';
import {Storage} from "@ionic/storage";

@Component({
  selector: 'app-top',
  templateUrl: './top.page.html',
  styleUrls: ['./top.page.scss'],
})
export class TopPage implements OnInit {
  private best:string = "";
  private worst:string = "";
  constructor( private storage:Storage) {
    storage.get('records').then((result)=>{
        this.best =  `${result.best.hours}:${result.best.minutes}:${result.best.seconds}`;
        this.worst = `${result.worst.hours}:${result.worst.minutes}:${result.worst.seconds}`;
    });
   }

  ngOnInit() {
  }

}
