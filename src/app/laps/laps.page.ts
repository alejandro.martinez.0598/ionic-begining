import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ChronoService } from '../chrono.service';
import{MyTime} from '../MyTime';

@Component({
  selector: 'app-laps',
  templateUrl: './laps.page.html',
  styleUrls: ['./laps.page.scss'],
})
export class LapsPage implements OnInit {
  public list: MyTime[]=[]; 
  constructor(public chronoService: ChronoService) { 
    this.list = chronoService.list;
    console.log(chronoService.list);
    
  
  }

  ngOnInit() {
  }

}
