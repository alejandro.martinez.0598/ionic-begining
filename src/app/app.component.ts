import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    // {
    //   title: 'Home',
    //   url: '/home',
    //   icon: 'home'
    // },
    // {
    //   title: 'my mi',
    //   url: '/my-page',
    //   icon: 'home'
    // },
    // // {
    // //   title: 'image',
    // //   url: '/image',
    // //   icon: 'home'
    // // },
    // {
    //   title: 'video',
    //   url: '/video',
    //   icon: 'logo-youtube'
    // },
    // {
    //   title: 'for-buttons',
    //   url: '/for-buttons',
    //   icon: 'home'
    // },
    {
      title: 'chrono',
      url: '/chrono',
      icon: 'logo-youtube'
    },
    {
      title: 'top',
      url: '/top',
      icon: 'logo-youtube'
    },
    {
      title: 'laps',
      url: '/laps',
      icon: 'logo-youtube'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
