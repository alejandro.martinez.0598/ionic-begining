import { NgModule ,CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FirstFilterPipe } from '../first-filter.pipe';
import { MonComposantComponent } from '../mon-composant/mon-composant.component';
import { JsonConsumerService } from '../json-consumer.service';

@NgModule({
  declarations: [FirstFilterPipe,MonComposantComponent],
  imports: [
    CommonModule
  ],
  exports:[
    FirstFilterPipe,
    MonComposantComponent
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class SharedModule { }
