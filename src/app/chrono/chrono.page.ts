import { Component, OnInit } from '@angular/core';
import { Events } from '@ionic/angular';
import { ChronoService } from '../chrono.service';
import { MyTime } from '../MyTime';
@Component({
  selector: 'app-chrono',
  templateUrl: './chrono.page.html',
  styleUrls: ['./chrono.page.scss'],
})
export class ChronoPage implements OnInit {
  public seconds =0;
  public minutes =0;
  public hours = 0;
  public interval;
  public runing : Boolean= true;
  public timeText: string ="";

  constructor(public chronoService:ChronoService) { 
   
  }
  

  ngOnInit() {

   
  }
 public start(){
   if(this.runing){
    this.interval = setInterval(()=>{
      this.seconds++;
      if(this.seconds>=60){
        this.minutes++;
        this.seconds = 0;
      }
      if(this.minutes>=60){
        this.hours++;
        this.minutes = 0;
      }
      this.timeText = `${this.hours}:${this.minutes}:${this.seconds}`
  
  },1000);
   }
   else{
     clearInterval(this.interval);
   }
   this.runing = !this.runing;
   
  }
  public stop(){}
  public lap(){
    let temp:MyTime ={"seconds":this.seconds,"minutes":this.minutes,"hours":this.hours}
   this.chronoService.addToList(temp);
  } 
  public reset(){}

}
