import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChronoPage } from './chrono.page';

describe('ChronoPage', () => {
  let component: ChronoPage;
  let fixture: ComponentFixture<ChronoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChronoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChronoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
