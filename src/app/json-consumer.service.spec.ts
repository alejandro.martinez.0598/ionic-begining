import { TestBed } from '@angular/core/testing';

import { JsonConsumerService } from './json-consumer.service';

describe('JsonConsumerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JsonConsumerService = TestBed.get(JsonConsumerService);
    expect(service).toBeTruthy();
  });
});
