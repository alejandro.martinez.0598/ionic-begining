import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'firstFilter'
})
export class FirstFilterPipe implements PipeTransform {

  transform(list: any[],filter: string): any[] {
    return list.filter(item => item.title.indexOf(filter)!=-1);
  }

}
