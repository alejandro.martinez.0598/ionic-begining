import { Injectable } from '@angular/core';
import { MyTime } from './MyTime';
import { Storage} from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class ChronoService {
  public list :MyTime [] = []
  private worst:MyTime = {"hours":0,"minutes":0,"seconds":0}
  private best:MyTime = {"hours":60,"minutes":60,"seconds":60}
  constructor( private storage: Storage) { }
  public getList(){
    return this.list;
  }
  public addToList(time:MyTime){
    if(time.hours>this.worst.hours) this.worst = time;
    else if (time.hours == this.worst.hours)
    if(time.minutes>this.worst.minutes) this.worst = time;
      else if (time.minutes == this.worst.minutes)
      if(time.seconds>this.worst.seconds) this.worst = time;

      if(time.hours<this.best.hours) this.best = time;
      else if (time.hours == this.best.hours)
      if(time.minutes<this.best.minutes) this.best = time;
        else if (time.minutes == this.best.minutes)
        if(time.seconds<this.best.seconds) this.best = time;

    this.list.push(time);
    this.saveRecords();
  }
  private saveRecords(){
    this.storage.set('records',{'best':this.best,'worst':this.worst})
  }
}
