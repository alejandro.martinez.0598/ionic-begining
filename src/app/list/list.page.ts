import { Component, OnInit } from '@angular/core';
import { JsonConsumerService } from '../json-consumer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Movie } from '../movie';


@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {

    private selectedItem: any;
    public search  ="back";
    public type = "movie";
    public list :Movie [] = []
    public page:number = 1;

  constructor( private router: Router,private consumer:JsonConsumerService, private rout: ActivatedRoute) {
    this.rout.params.subscribe(params=>{
      console.log(params);
      this.search = params.text;
      this.type = params.type;
    })
  }

  ngOnInit() {
        let url =  `http://www.omdbapi.com/?apikey=cda8e640&s=${this.search}&type=${this.type}&page=${this.page}`
        this.consumer.getFrom( url).subscribe(response=>{
          console.log(response);
          this.list = response.Search
          this.page++;
      } );
   
  }
  public showDetailsOf(index){
    this.router.navigate(["/image",this.list[index]]);
      console.log(index);
      
  }





  public loadData(event){
    console.log("load data triguered", event);
    let url =  `http://www.omdbapi.com/?apikey=cda8e640&s=${this.search}&type=${this.type}&page=${this.page}`
        this.consumer.getFrom( url).subscribe(response=>{
          console.log(response);
          
          this.list.push(...response.Search);
          
          this.page++;
          event.target.complete();
      } );
    
  }
}
