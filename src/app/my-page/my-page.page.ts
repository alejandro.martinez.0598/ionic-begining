import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-my-page',
  templateUrl: './my-page.page.html',
  styleUrls: ['./my-page.page.scss'],
})
export class MyPagePage implements OnInit {

  constructor(private router: Router,private rout : ActivatedRoute){
    // reupera los parametros enviados por la pagina anterior   
    this.rout.params.subscribe(params=>{
        console.log(params);
        
      })
  }

  ngOnInit() {
  }
  /**
   * method
   */
  public method() {
    console.log("3456789087654rt");
    
  }

}
